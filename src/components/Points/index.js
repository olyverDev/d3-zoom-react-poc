import React, { Fragment, memo } from 'react';
import PropTypes from 'prop-types';

import './Points.css';

const Points = ({ set, radius }) => (
  <Fragment>
    {set.map(({ ID, x, y }) => (
      <circle
        key={ID}
        className="circle"
        cx={x}
        cy={y}
        r={radius}
      />
    ))}
  </Fragment>
);

Points.propTypes = {
  radius: PropTypes.string,

  set: PropTypes.arrayOf(PropTypes.shape({
    ID: PropTypes.number.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
  })),
}

Points.defaultProps = {
  radius: '5',
}

export default memo(Points);
