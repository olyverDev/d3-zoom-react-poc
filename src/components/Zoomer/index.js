import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Zoomer.css';

import { ZOOMER_SCALE } from '../../constants';

class Zoomer extends Component {
  static propTypes = {
    scaleConfig: PropTypes.shape({
      min: PropTypes.number.isRequired,
      max: PropTypes.number.isRequired,
      step: PropTypes.number.isRequired,
    }),
    scale: PropTypes.number,

    onChange: PropTypes.func,
  }

  static defaultProps = {
    scale: ZOOMER_SCALE.min,

    scaleConfig: ZOOMER_SCALE,
  
    onChange: () => {},
  }

  static getDerivedStateFromProps(props, state) {
    if (props.scale !== state.scale) {
      return { scale: props.scale };
    }

    return null;
  }

  state = {
    scale: this.props.scale,
  }

  getScaledWidth = scale =>
    Math.round((scale * 100) / this.props.scaleConfig.max);

  decrease = () => {
    const { step, min } = this.props.scaleConfig;
  
    if (this.state.scale > min) {
      const nextScale = Math.max(min, this.state.scale - step);
      this.setState({ scale: nextScale });
      this.props.onChange(nextScale);
    }
  }

  increase = () => {
    const { step, max } = this.props.scaleConfig;

    if (this.state.scale < max) {
      const nextScale = Math.min(max, this.state.scale + step);
      this.setState({ scale: nextScale });
      this.props.onChange(nextScale);
    }
  }

  render() {
    return (
      <div className="zoomer">
        <div
          className="control decrease"
          onClick={this.decrease}
        >
          -
        </div>
        <div className="scale">
          <div
            style={{ width: `${this.getScaledWidth(this.state.scale)}%` }}
            className="progress"
          />
        </div>
        <div
          className="control increase"
          onClick={this.increase}
        >
          +
        </div>
      </div>
    );
  }
}

export default Zoomer;
