import React, { Component } from 'react';
import { zoom } from 'd3-zoom';
import { select, event } from 'd3-selection';

import './App.css';

import Zoomer from './components/Zoomer';
import Points from './components/Points';

import {
  PLOT_HEIGHT,
  PLOT_WIDTH,
  D3_ZOOM_MIN_SCALE,
  D3_ZOOM_MAX_SCALE,
  DIMENSIONS,
} from './constants';

import { createPoints } from './util/points';
import { toD3ZoomScale, toZoomerScale } from './util/context';

const POINTS = createPoints({ count: 5000 });

class App extends Component {
  selection = null;

  svg = React.createRef();

  zoomable = React.createRef();

  state = {
    scale: D3_ZOOM_MIN_SCALE,
  }

  componentDidMount() {
    this.selection = select(this.svg.current);
    this.zoom = zoom();
    this.selection.call(
      this.zoom
        .extent(DIMENSIONS)
        .translateExtent(DIMENSIONS)
        .scaleExtent([D3_ZOOM_MIN_SCALE, D3_ZOOM_MAX_SCALE])
        .on('zoom', () => {
          this.zoomable.current.setAttribute('transform', event.transform);
          this.setState({ scale: event.transform.k });
        }),
    );
  }

  handleZoomChange = (scale) => {
    const d3ZoomScale = toD3ZoomScale(scale);
    this.zoom.scaleTo(this.selection, d3ZoomScale);
    this.setState({ scale: d3ZoomScale });
  }

  render() {
    return (
      <div className="App">
        <Zoomer
          scale={toZoomerScale(this.state.scale)}
          onChange={this.handleZoomChange}
        />
        <div className="zoomableContainer">
          <svg
            ref={this.svg}
            className="zoomable"
            height={PLOT_HEIGHT}
            width={PLOT_WIDTH}
          >
            <rect className="colorArea" />
            <g ref={this.zoomable} className="points">
              <Points set={POINTS} />
            </g>
          </svg>
        </div>
      </div>
    );
  }
}

export default App;
