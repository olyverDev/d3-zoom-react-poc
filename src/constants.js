
export const PLOT_HEIGHT = 600;
export const PLOT_WIDTH = 800;
export const DIMENSIONS = [[0, 0], [PLOT_WIDTH, PLOT_HEIGHT]];

export const D3_ZOOM_MIN_SCALE = 1;
export const D3_ZOOM_MAX_SCALE = 11;

export const ZOOMER_SCALE_DIVISION_COUNT = 10;
export const ZOOMER_SCALE_STEP = 1;

export const ZOOMER_SCALE = {
  min: 0,
  max: ZOOMER_SCALE_DIVISION_COUNT,
  step: ZOOMER_SCALE_STEP,
};

