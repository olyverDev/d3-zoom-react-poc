import { D3_ZOOM_MIN_SCALE, D3_ZOOM_MAX_SCALE } from '../constants';

export const toZoomerScale = d3ZoomScale => d3ZoomScale - D3_ZOOM_MIN_SCALE;
export const toD3ZoomScale = zoomerScale =>
  Math.min(zoomerScale + D3_ZOOM_MIN_SCALE, D3_ZOOM_MAX_SCALE);