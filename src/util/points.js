import { PLOT_WIDTH, PLOT_HEIGHT } from '../constants';

const createRangeRandomizer = (min = 1, max = 1000) =>
  () => Math.random() * (max - min + 1) + min;

const randomizeX = createRangeRandomizer(0, PLOT_WIDTH);
const randomizeY = createRangeRandomizer(0, PLOT_HEIGHT);

export const createPoints = ({ count }) => {
  const points = [];
  for (let i = 0; i < count; i++) {
    points.push({
      ID: i,
      x: randomizeX(),
      y: randomizeY(),
    })
  }

  return points;
}
